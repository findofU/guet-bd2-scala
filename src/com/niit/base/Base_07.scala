package com.niit.base

import scala.collection.mutable.ArrayBuffer

object Base_07 {
    //定义人类
  class Person{
      var name=""
      var age=0
      def eat()=println("吃东西")
    }
  //定义老师类
  class Teacher extends Person

  //定义学生类
  class Student extends Person{
    //重写方法
    override def eat(): Unit = println("索粉")
  }

  /*
  * 抽象类
  *   abstract  一个类中，如果有抽象字段或者抽象方法 该类一定是抽象类
  * */
  //定义一个抽象的形状
  abstract class Shape{
    def area:Double
  }
  //定义一个正方形
  class Square(var edge:Double) extends Shape{
    override def area: Double = edge * edge
  }
  //定义一个长方形
  class Rectangle(var length:Double,var width:Double) extends Shape{
    override def area: Double = length * width
  }

  /*
  特质：关键字 trait 修饰。
  特点：
特质可以提高代码的复用性。
特质可以提高代码的扩展性和可维护性。
类与特质是继承关系，类与类只支持单继承，类与特质之间可以单继承也可以多继承。
Scala 的特质中可以有普通字段、抽象字段、普通方法、抽象方法。
如果特质只有抽象内容也叫瘦接口，如果既有抽象内容又有具体内容叫做富接口。
   */
  trait Logger{
    def log(msg:String)
  }
  trait File{
    def write(msg:String)
  }

  class ProgramLogger extends Logger with File{
    override def log(msg: String): Unit = println(msg)

    override def write(msg: String): Unit = println(msg)
  }

//样例类  case
  case class People(name:String="张三",var age:Int){}


  def main(args: Array[String]): Unit = {

    //定长数据
    val arr1 = new Array[Int](5)
    val arr2 = Array(1,2,3,4,5)
    println(arr1.length,arr2.size)
    println(arr2(2))
    //变长数组
    import scala.collection.mutable.ArrayBuffer
    val ab1 = ArrayBuffer[Int]()
    val ab2 = ArrayBuffer("Java","JavaScript","Python","Scala")
    println(ab1,ab2)
    //增删改元
    //添加元素
    ab1 += 999
    ab1 += 100
    ab1 += 231
    println(ab1)
    //删除元素
    ab1 -= 100
    println(ab1)
    //添加多个元素
    ab1 ++= Array(1,2,3)
    println(ab1)
    ab1 --= Array(1,2,3)
    println(ab1)
    //遍历元素
//    for (i <- 0 to ab2.length -1){
//      println(ab2(i))
//    }
//    for (i <- 0 until ab2.length){// until 是一个左闭右开的区间
//      println(ab2(i))
//    }
    //直接遍历数组元素
    for (i <- ab2){
      println(i)
    }

//    val p =  People("李四",18)
//    //p.name = "老刘"
//    p.age = 20
//    println(p)
//
//    val pl = new ProgramLogger()
//    pl.log("日志打印")
//    pl.write("文件写入")
//
//    val sq = new Square(10)
//    println(sq.area)
//    val re:Shape = new Rectangle(10,5)
//    println(re.area)
//
//
//    val teacher = new Teacher;
//    teacher.name="张三"
//    teacher.age = 28
//    teacher.eat()
//    val stu1 = new Student()
//    stu1.name = "李四"
//    stu1.age = 18
//    stu1.eat()
//    //多态：多态的前提是继承，两个有继承关系的类，可以用父类去接收子类
//    val stu2:Person = new Student();
//    stu2.eat()

  }

}
