package com.niit.base

object Base_10 {

  def main(args: Array[String]): Unit = {
    /*
    Option类型：在我们实际的开发中，会遇到空指针异常。在Scala中可以使用Option类型去避免这些异常
     Some:表示实际的值
     None：表示没有值
     */
    var a =10;
    var b = 0
    var c = div(a,b)
    //println(c)
    c match {
      case Some(x) => println(x)
      case None => println("分母不能为0")
    }

    /*
    柯里化 ：在Scala中和Spark中都大量使用到了柯里化，方便后面我们读写代码，我们有必要了解下柯里化，


     */
    val str1 = merge1("abc", "efg")
    println(str1)

    val str2 = merge2("xyz", "jkl")(_ + _)
    println(str2)
                      //参数                   在后面的括号里面写具体逻辑
    val str3 = merge2("xyz", "jkl")(_.toUpperCase concat  _)
    println(str3)


    var stu = Student("张三",23);
    println(stu.name,stu.age)

    //使用提取器的方式
    var res = Student.unapply(stu)
    println(res)
  }
  /*
   提取器：apply -- unapply:在伴生关系下，将对象中属性进行提取
   * */
  //伴生类
  class Student(var name:String,var age:Int)
  //伴生对象
  object Student{
    def apply(name: String, age: Int): Student = new Student(name, age)

    def unapply(arg: Student): Option[(String, Int)] = {
      //相当于把对象 拆解成 各个属性
      if (arg !=null){
        Some(arg.name,arg.age)
      }else{
        None
      }
    }
  }


  //传统方式
  def merge1(s1:String,s2:String) = s1 + s2

  //柯里化方式 (s1:String,s2:String)
  def merge2(s1:String,s2:String)(f1:(String,String)=>String) = f1(s1,s2)



  def div(a:Int,b:Int)={
    if(b == 0){
      None
    }else{
      Some(a/b)
    }
  }

}
