package com.niit.base

import scala.io.StdIn

object Base_09 {

  def main(args: Array[String]): Unit = {
    /*
    模式匹配
        1.简单模式匹配
        2.匹配类型
        3.守卫模式匹配
        4.匹配样例类
        5.匹配集合
        6.变量声明中的模式匹配
        7.匹配for表达式
     */
    //1.简单模式匹配
    /*
        输入 hadoop 或者 spark 分别显示这个两个软件的简介信息 如果输入不是hadoop也不是spark那么就提示未匹配
    * */
//    println("请输入软件名称：")
//    var str =StdIn.readLine()
//    /*
//      switch(xxx){
//        case XXXX
//      }
//     */
//   var res =  str match {
//      case "hadoop" => "大数据分布式存储和计算框架"
//      case "spark" => "大数据分布式内存计算框架"
//      case _ => "未匹配"
//    }
//
//    println(res)


    //2.类型模式匹配
    var i:Any = false
    var res = i match {
      case i:String => "当前值为字符串"
      case i:Int => "当前值为整型"
      case i:Double => "当前值为小数"
      case _ => "未匹配"
    }
    println(res)

    //3.守卫模式匹配
    //需求：判断某个值 在那个范围内
    var num = 10
    num match {
      case a if a>=0 && a<=10 => println("当前值在0-10的范围内")
      case a if a>= 11 && a<=20 => println("当前值在11-20范围内")
      case _ => println("未匹配")
    }

    /*4.样例类匹配

    样例类型后的小括号中，编写的字段个数要和样例类的字段个数保持一致
    通过match进行模式匹配的时候，要匹配的对象必须声明为Any类
     */
    val c:Any = Customer("张三",18)
    val o:Any = Order(112233)
    val arr:Any = Array(0,1)

    arr match {
      case Customer(a,b) => println("这是Customer类型")
      case Order(c) =>   println("这是Order类型")
      case _ => println("未匹配")
    }

    /*
    5.集合模式匹配
     */
    //1数组模式匹配
    var arr1 = Array(1,2,3)
    var arr2 = Array(0)
    var arr3 = Array(1,2,3,4,5)
    //通过模式匹配，找到指定数据
    arr1 match {
        //匹配长度为3，且首元素为1
      case Array(1,x,y) => println("匹配长度为3，且首元素为1的数组")
        //匹配只有一个0元素的数据
      case Array(0) => println("匹配只有一个0元素的数据的数组")
      //匹配首元素为1,且长度不固定
      case Array(1,_*) => println("匹配首元素为1,且长度不固定的数组")
      case _ => println("未匹配")
    }
    //2列表模式匹配
    var list1 = List(2,3)
    var list2 = List(0)
    var list3 = List(1,2,3,4,5)
    //通过模式匹配，找到指定数据
    //方式一
    list3 match {
      //匹配长度为2
      case List(x,y) => println("匹配长度为2的数组")
      //匹配只有一个0元素的数据
      case List(0) => println("匹配只有一个0元素的数据的数组")
      //匹配首元素为1,且长度不固定
      case List(1,_*) => println("匹配首元素为1,且长度不固定的数组")
      case _ => println("未匹配")
    }
    //方式二 用列表中特有的关键字
    list3 match {
      //匹配长度为2
      case x::y::Nil => println("匹配长度为2的数组")
      //匹配只有一个0元素的数据
      case 0::Nil => println("匹配只有一个0元素的数据的数组")
      //匹配首元素为1,且长度不固定
      case 1::tail => println("匹配首元素为1,且长度不固定的数组")
      case _ => println("未匹配")
    }
    //6.变量声明中模式匹配
    /*
    需求：1.生成1-10的数组，使用模式匹配获得 第 二 三 四 元素
          2.生成1-10的列表，使用模式匹配获得 第一 二 元素
     */
    //生成1-10的数组，使用模式匹配获得 第 二 三 四 元素
    var arr4 = (1 to 10).toArray
    val Array(_,x,y,z,_*) = arr4
    println(x,y,z)

    //生成1-10的列表，使用模式匹配获得 第一 二 元素
    var list4 = (1 to 10).toList
    var List(a,b,_*) = list4
    println(a,b)
    var t::d::tail = list4
    println(t,d)

    /*
    7.匹配for循环表达式
    需求：定义变量记录学生的姓名和年龄
          获得所有年龄为23的学生信息进行打印
     */
    val map1 = Map("张三"->23,"李四"->24,"王五"->25,"马六"->26)
    //传统方式
    for ((k,v) <- map1 if v==23){
      println(k,v)
    }
    //模式匹配方式
    for ((k,23) <- map1){
      println(k,23)
    }
  }
  case class Customer(var name:String,var age:Int)
  case class  Order(var id:Int)

}
