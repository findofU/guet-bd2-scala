package com.niit.base

import scala.io.StdIn

/*
*  1.类型转换
*     自动类型转换：范围大的数据类型可以接收范围小的数据类型。 大的可以装小的
*     强制类型转换：范围小的数据乐行去接收范围大的数据类型。   小的去装大的
*           格式：val/var 变量名：数据类型 = 数值.toXxx  (Xxx:要转换的数据类型)
*           强制类型转换会存在损失精度的问题
*     值类型转换为字符串类型
*           格式一：val/var 变量名:String  = 值类型 +""
*           格式二：val/var 变量名:String  = 值类型.toString
*     字符串类型转为值类型
*            格式：val/var 变量名：数据类型 = 字符串.toXxx  (Xxx:要转换的数据类型)
*   2.键盘录入
        格式：StdIn.readXxx()    (Xxx：要接收的数据类型)
        接收字符串的格式：StdIn.readLine()
        接收整数的格式：StdIn,readInt()\
    3.算数运算符
      包括 +（加号）、-（减号）、*（乘号）、/（除号）、%（取余）。
      Scala中没有 ++，--这两个运算符。
      a % b，底层是 a - a/b * b
      Scala 中把字符串和整数数字 n 相乘相当于让字符串重复 n 次。
    4.赋值运算符
       基本赋值运算符 =
      扩展赋值运算符 +=、-=、*=、/=、%=
    5.关系运算符
      包括>、>=、<、<=、==、!=
      最终结果一定是 true 或 false。
      如果需要比较数据值，使用==或者!=，如果比较引用值需要使用 eq 方法。
    6.逻辑运算符
      包括 &&、||、!
      &&：两者都成立则返回true，其中有一个不成立返回false
      || ：其中有一个为true,则返回为true
       ！：相反
    最终结果一定是 true 或 false。
    7.顺序结构 程序按照从上至下、从左至右的顺序依次逐行执行，中间没有任何判断和跳转。
    8.选择结构
        if语句：判断条件是否成立，如果成立则执行大括号里面的语句
        if-else语句：如果if中条件成立则执行if大括号中的代码块，如果不成立则执行else大括号中的代码块
        if-else if -else语句：如果if中条件成立则执行if大括号中的代码块，否则进入到一下else if语句进行判断，如果都不成立则执行else大括号中的代码块
       替换三元运算符  int a = 2 > 1? 2 : 1  Scala中没有三元运算符   val res = if(2>1) 2 else 1
* */
object Base_03 {

  def main(args: Array[String]): Unit = {
//   1.1类型转换 --自动类型转换 范围大的数据类型可以接收范围小的数据类型。
//    Byte - Short (Char)- Int - Long - Float - Double
    var a:Int  = 3
    var b:Double = a + 3.14
    println(b)
//    1.2类型转换 --- 强制类型转换 范围小的数据乐行去接收范围大的数据类型。
//    格式：val/var 变量名：数据类型 = 数值.toXxx  (Xxx:要转换的数据类型)
//    强制类型转换会存在损失精度的问题
    val c:Double =5.21
    a  = c.toInt
    println(a)
//  1.3值类型转换为字符串类型
//    Java：任意数据类型拼接一个字符串 最终都会变成一个字符串
//    格式一：val/var 变量名:String  = 值类型 +""
    val d:String = a + ""
    println(d)
//   格式二：val/var 变量名:String  = 值类型.toString
    val e:String = a.toString
    println(e)
//    1.4字符串类型转为值类型
//    格式：val/var 变量名：数据类型 = 字符串.toXxx  (Xxx:要转换的数据类型)
    val f:String = "true"
    val g:String= "2.333"
    val h:Boolean = f.toBoolean
    val i:Double = g.toDouble
    println(h,i)
//    键盘录入
//    格式：StdIn.readXxx()    (Xxx：要接收的数据类型)
//    接收字符串的格式：StdIn.readLine()
//    接收整数的格式：StdIn,readInt()\
//    println("你的人生梦想是什么？")
//    val str = StdIn.readLine();
//    println(s"我的人生梦想是：${str}")
//    3.算符运算符
    var j = 27 % 5  // 27 - 27/5 * 5 =27 - 5 * 5  = 27 - 25 = 2  a - a/b * b
    println(j)
    println("a"*3)
//    4.赋值运算符
    var k = 7;var l = 3
    k += l // k = k + l  7 + 3
    println(k)
//    5.关系运算符
//    println( 1 == 2) //false
//    println("abc".eq("efg")) //false
//    println(!"abc".eq("efg")) // !false = true
//    6.逻辑运算符
//    println(true && true) //true
//    println(true && false) //false
//    println( 1 > 2 && 2 < 3 ) //false
//    println( 1>2 &&  3 > 9)//false
//
//    println( 1 > 2 || 2 < 3 ) // true
//    println( 1>2 || 3 > 9)// false
//    println( 1>0 || 9>8) //true

    println( !(1 > 2 || 2 < 3) ) // false
    println( !(1>2 || 3 > 9))// true
//    7. if语句
//    if(1 < 0){
    ////      println("1>0，成立了")
    ////    }
    //////    8.if-else语句
    ////    var score = 60.5
    ////    if( score >= 60){
    ////      println("及格了")
    ////    }else{
    ////      println("不及格")
    ////    }
//    9.if - else if -else
      var score = -1
    if(score >=90 && score <=100){
      println("你很优秀")
    }else if(score >=80 && score <90){
      println("一般般")
    }else if(score >=0 && score <80){
      println("无药可救")
    }else{
      println("成绩无效")
    }
//    替换三元运算符  int a = 2 > 1? 2 : 1  Scala中没有三元运算符
    val res = if(2>1) 2 else 1
    println(res)









  }

}
