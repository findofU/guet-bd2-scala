package com.niit.base

object Base_05 {

//  def main(args: Array[String]): Unit = {
//    //1.方法的调用
//    //1.1后缀调用法  对象名.方法名(参数)
//      var abs = Math.abs(-10)
//      println(abs)
//    //1.2中缀调用法  对象名 方法名 参数
//     var l = 1 to 10
//    abs = Math abs -10
//    println(abs)
//    //在 Scala 中所有的操作符都是方法，操作符是一个方法名，是符号的方法。+ - * / %
//    var i = 1 + 1
//    //1.3花括号调用法 类似于块表达式，可以｛｝中写很多表达式，且最后一个表达式为该方法的返回值
//    abs = Math.abs{
//      println("-10")
//      if(2>1){
//        println("成立了")
//      }
//      -10
//    }
//    println(abs)
//    //1.4无括号调用法 如果方法没有参数，可以省略方法后面的括号 不建议使用
//    sayHello
//
//    //2.函数 val 函数变量名 = (参数名:参数类型,参数名:参数类型.....)=> 函数体
//    /*
//    注意： 在Scala中，函数是一个对象
//            类似于方法，函数也是有参数列表和返回值
//            函数定义不需要使用def关键字
//            无需指定返回类型
//     */
//    var fun1 = (x:Int,y:Int)=>println(x,y)
//    fun1(1,2)
//    var fun2 = (x:Int,y:Int)=> x+y
//    var res = fun2(2,3)
//    println(res)
//    /*
//    函数与方法的区别：
//      方法属于类或者对象，在运行时会加载到 JVM 的方法区。
//      可以将函数对象赋值给一个变量，在运行时会加载到 JVM 的堆中。
//      函数是一个对象，继承自 FunctionN，函数对象有apply、curried、toString、tupled这些方法，方法则没有。
//      结论：在 Scala 中，函数是对象，而方法是属于对象的，可以理解为：方法归属于函数
//       函数 大于 方法
//       函数 包含 方法
//       自动类型转换：用大的 去接 下的
//     */
//    //方法转换为函数  val/var 变量名 = 方法名 _
//    var ss = sayHello _
//    println(ss())
//    var ss1 = getSum _
//    res = ss1(1,2)
//    println(res)
//  }


//  def sayHello()={
//    println("Hello,Scala")
//    999
//  }
//
//  def getSum(x:Int,y:Int)= x+y

    //什么是类：看做是一个模板 看做一个图纸  —> 很多的房子
    //什么是对象：根据类 去 创建的 真实物体
  def main(args: Array[String]): Unit = {
      //将类变成一个真实的东西  实例化 new
      //空构造器可以省略括号
      val p = new Person();
      //对象名.属性的方式去访问
      p.name = "张三"
      p.age = 18
      p.address = "广西北海"
      p.phone=176666666
      //println(p.name,p.age,p.address,p.phone)
      p.printInfo()
      p.printMsg("Say Hello")

      val s = new Student()
      s.setName("李四")
      s.setAge(16)
      println(s.getName(),s.getAge())
      //s.printMsg("你好")
  }
  //空类 可以省略大括号
  class Person {
    //定义成员变量,必须要有初始值
    var name = ""
    var age = 0;
    //空格_ 进行初始值占位，如果想使用占位符，必须要指定数据类型
    var address:String = _
    var phone:Int = _

    //定义成员方法
    def printInfo()={
      println(name,age,address,phone)
    }

    def printMsg(msg:String)={
      println(msg)
    }
  }

  class Student{
    private var name:String= _
    private var age:Int = _

    def getName() = name
    def setName(name:String) = this.name = name
    def getAge()={
      age
    }
    def setAge(age:Int)={
      this.age = age
    }
    private def printMsg(msg:String)={
      println(msg)
    }

  }
}
