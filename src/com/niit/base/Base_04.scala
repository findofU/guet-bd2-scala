package com.niit.base


/*
* 1.块表达式:的是用 {  代码  }，一组的代码。在Scala代码块也是有返回值的。且最有一句是返回值，return可以省略。默认返回一个空元组
* 2.
*/

object Base_04 {

  def main(args: Array[String]): Unit = {
    //1.块表达式：指的是用 {  代码  }，一组的代码。在Scala代码块也是有返回值的。且最有一句是返回值，return可以省略
//    var a = {//2 <- 6
//      println("1 + 1") //必然执行
//      if(2>1){
//        println("成立了")
//      }
//      //默认返回一个空元组
//      1+1
//      3+3
//      ""
//    }
//    println(a)
    //2.for循环
    //2.1简单for循环，格式：for(i <- 数组/集合){代码}
    //需求：向控制台输出10 Hello Scala
//    var nums = 1 to 10
//    for (i <- nums){//[1,10]
//      println("Hello,Scala",i)
//    }
    //2.1for循环的守卫：在for循环的表达式中，可以添加if判断语句，if称之为守卫。
    //需求：使用for循环打印1-10之间可以被3整除的数字
//    for (i <- 1 to 10 if i % 3==0){
//      println(i)
//    }
    //2.2for循环的推导式：Scala 中的 for 循环也有返回值，可以使用 yield 表达式构建出一个集合。
    //生成 10、20-100的集合
//    val v = for (i <- nums) yield i*10
//    println(v)

    //3while  do-while循环
    //打印1-10
//    var i = 1
//    while(i<=10){
//      println(i)
//      i += 1
//    }
//    do {
//      println(i)
//      i += 1
//    }while(i<=10)

    //4.break
    // 在 Scala 中，移除了 break 和 continue 关键字
    //如果要使用，需要 scala.util.controle 包下的 Breaks 类的 breakable 和 break 方法
    //导包： import scala.util.control.Breaks._
    //使用 breakable 将 for 表达式包起来
    //for 表达式中需要退出循环的地方，添加 break() 方法调用。
    //需求：例如输出 1-10 的数字，遇到5退出
    import scala.util.control.Breaks._
//    breakable{
//      for (i <- 1 to 10){
//        if (i == 5){
//          break()
//        }else{
//          println(i)
//        }
//      }
//    }
  //------------------------------
    /*
    continue 的实现和 break 类似，不同的是需要使用 continue 的地方是用 breakable 将 for 表达式的循环体包起来即可。
    例如输出 1-10 不能整除 3 的数字：
    * */
//    for (i <- 1 to 10){
//      breakable{
//        if( i % 3==0 ){
//          break()
//        }else{
//          println(i)
//        }
//      }
//    }


    //5.1方法：
    // 格式 ：def 方法名(参数名1:参数类型1,参数名2:参数类型2...)[: 返回值类型] = {
    //          方法体
    //        }
    //参数列表的参数类型不能省略
    //返回值类型可以省略，由 Scala 编译器自动推断
    //返回值可以不写 return ，默认就是块表达式的
    //示例：定义一个方法用来获取两个整形数字的最大值并返回结果。
    //调用方法
    var max = getMax(b=10,a =20); //10，20：实参(实际上的参数) 需要参加真正的逻辑运算的数据
    println(max)

    //5.2方法参数
    var sum = getSum(1,2,3,4,5,6,7,8,9,10,11,12,13)
    println(sum)
  }

  //方式一：标准版   a:Int,b:Int :形参 （形式上的参数） 占位的作用
  def getMax(a:Int = 1,b:Int = 0):Int={ // a:10  b:20
    return if(a > b) a else b
  }
  //变长参数
  //格式：def 方法名(参数名:参数类型*)[: 返回值类型] = {
  //	  方法体
  //  }
  //传入 n个数 进行求和  a:Int*:可以理解为是一个整型数组
  def getSum(a:Int*)={
    a.sum
  }
}
