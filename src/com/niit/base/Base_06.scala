package com.niit.base

//构造器：当创建对象的时候，会自动调用类的构造器。创建类的时候可以进行初始值的传递

object Base_06 {

  //Scala定义有参构造器 class 类名(var/val 参数名：类型,var/val 参数名：类型=默认值){}
              //主构造器 ，一旦定义了有参构造器，那么无参构造器就失效了
  class Student(var name:String,val age:Int =18){
        //辅助构造器  （主构造器也传入了姓名和年龄，但是我有的时候也想要住址）
        //格式 ： def this(参数列表){}  辅助构造器的名字 必须叫this 注意：辅助构造器的第一行代码 必须调用主构造器或者其他构造器
    def this(name:String,age:Int,address:String){
      this(name,age)//主构造器或者其他构造器
      println("辅助构造器的内容：",name,age,address)
    }
    println(name,age)
  }

  //static:静态 直接通过类名. 直接去访问。在Scala中是没有静态的，只能使用单例对象去做 适用于  工具对象
  object Dog{
    val leg_num = 4

    def jiao(): Unit ={
      println("汪"*15)
    }
  }

  //java中，经常有一些 既然普通（实例）成员 又有静态成员。那么在Scala中是没有办法做到的，所以就有伴生对象
  /*特点： 对象和类名是相同的，那么二者就是伴生关系 object称之为伴生对象 class称之为伴生类
            必须在同一个文件当中
            伴生对象和伴生类可以互相访问private修饰的成员
   */

  class Generals{
    def toWar(): Unit ={
      println("手握"+Generals.armsName)
     // Generals.printArms()
    }
  }
  //互为伴生
  object Generals{
    private var armsName = "AK47"

    private[this] def printArms(): Unit ={
      println("哒"*15)
    }
  }

  //apply：在Scala中，支持创建对象的时候，不使用new关键字进行创建，通过apply的方式进行创建对象
  //必须要伴生对象中去定义
  /*
      object 名称{
        def apply(参数名：类型......) = new 伴生类（参数）
      }
   */
  class Person(var name:String,var age:Int){}

  object Person{
    //定义apply方法，实现创建Person对象的时候 免new
    def apply(name: String, age: Int): Person = new Person(name, age)
  }


  def main(args: Array[String]): Unit = {
    //val stu = new Student("张三",16);
    //val student = new Student("李四",12,"南宁")
//    println(Dog.leg_num);
//    Dog.jiao()
//      val g = new Generals;
//      g.toWar()
    val p = Person("张三",16)
    println(p.name,p.age)
  }

}
